include .env
export $(shell sed 's/=.*//' .env)

login: 
	docker login registry.gitlab.com

venv-setup:
	pipenv install --dev --skip-lock

code-config-setup: venv-setup
	python3 ./scripts/code_config.py $(shell pipenv --venv)


mongo-run:
	docker-compose up -d movie-graph-mongo

mongo-purge:
	$(info *** WARNING you are deleting all data from mongodb ***)
	docker-compose stop movie-graph-mongo
	sudo rm -r .$(MONGO_DATA_PATH)
	docker-compose up -d movie-graph-mongo

neo4j-run:
	docker-compose up -d movie-graph-neo4j

neo4j-stop:
	docker-compose stop movie-graph-neo4j

neo4j-purge:
	$(info *** WARNING you are deleting all data from neo4j ***)
	docker-compose stop movie-graph-neo4j
	sudo rm -r .$(NEO_DATA_PATH)
	docker-compose up -d movie-graph-neo4j


jupyter-build: venv-setup
	docker-compose build movie-graph-jupyter

jupyter-run: neo4j-run mongo-run
	docker-compose up movie-graph-jupyter-gpu

jupyter-run-cpu: neo4j-run
	docker-compose up movie-graph-jupyter-cpu


pbg-pull-image:
	docker pull $(PBG_EXECUTOR_IMAGE)

pbg-run:
	docker-compose run pbg-executor-gpu

pbg-run-cpu:
	docker-compose run pbg-executor-cpu

pbg-purge:
	$(info *** WARNING you are deleting all data from last pbg train ***)
	sudo rm -r resources/pbg/*


app-build: venv-setup
	docker-compose build movie-graph-app

app-run:
	docker-compose up movie-graph-app-cpu
