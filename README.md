# Movie Graph

_Movie recommender with graphs using [neo4j](https://neo4j.com/) and [PBG](https://github.com/facebookresearch/PyTorch-BigGraph)_
<br/>
<br/>

Setup
----

```bash
$ git clone https://gitlab.com/bastiansg_bas/movie-graph
```

This repo requires Docker 20.10 and docker-compose 1.28

If you want to run the jupyter notebooks or PBG on GPU you also need the latest version of [nvidia-container-toolkit](https://github.com/NVIDIA/nvidia-docker)

You can install nvidia-container-toolkit as follows:

```bash
$ distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
$ curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
$ curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

$ sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
$ sudo systemctl restart docker
```
<br/>

API
---

#### Build app
```bash
$ make app-build
```
#### Run app
```bash
$ make app-run
```
#### API documentation
    http://localhost:8000/docs
<br/>

Jupyter
-------

#### Build
```bash
$ make jupyter-build
```
#### Run on GPU
```bash
$ make jupyter-run
```
#### Run on CPU
```bash
$ make jupyter-run-cpu
```
