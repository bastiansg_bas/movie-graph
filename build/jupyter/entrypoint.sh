#!/bin/env sh

# install src packages
pip install --no-deps -e /src/movie_graph

jupyter-lab --ip=0.0.0.0 --allow-root --no-browser
