import os


TRAIN_PARTIONED_PATH = os.getenv('TRAIN_PARTIONED_PATH')
TEST_PARTIONED_PATH = os.getenv('TEST_PARTIONED_PATH')
TRAIN_FILE_PATH = os.getenv('TRAIN_FILE_PATH')
MODEL_PATH = os.getenv('MODEL_PATH')


def get_torchbiggraph_config():

    config = {
        # I/O data
        'entity_path': os.path.dirname(TRAIN_FILE_PATH),
        'edge_paths': [
            TRAIN_PARTIONED_PATH,
            TEST_PARTIONED_PATH,
        ],
        'checkpoint_path': MODEL_PATH,
        # Graph structure
        'entities': {
            'all': {
                'num_partitions': 1
            }
        },
        'relations': [
            {
                'name': 'all_edges',
                'lhs': 'all',
                'rhs': 'all',
                'operator': 'complex_diagonal',
            }
        ],

        'dynamic_relations': True,
        # Scoring model
        'dimension': 512,
        'global_emb': False,
        'comparator': 'dot',
        # Training
        'num_epochs': 50,
        'batch_size': 10000,
        'num_uniform_negs': 1000,
        'loss_fn': 'softmax',
        'lr': 0.1,
        'regularization_coef': 1e-3,
        # Evaluation during training
        'eval_fraction': 0,
        # Use GPU
        'num_gpus': 1
    }

    return config
