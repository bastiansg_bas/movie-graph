WITH $imdb_ids AS movies, $similar_ids AS similars 

WITH movies, similars,
    [(m:Movie)-->(n)
        WHERE m.id IN movies | id(n)] AS n1

WITH similars, n1,
	[(m:Movie)-->()-[:SIMILAR_TO_TAG]->(n)
        WHERE m.id IN movies | id(n)] AS n2

WITH similars,
    apoc.coll.toSet(n1 + n2) AS c1

WITH c1, similars,
    [(m:Movie)-->(n)
        WHERE m.id IN similars
        AND m.imdb_vote_average >= $min_rating
        AND id(n) IN c1 | m] AS m1

WITH c1, m1,
    [(m:Movie)-->()-[:SIMILAR_TO_TAG]->(n)
        WHERE m.id IN similars
        AND m.imdb_vote_average >= $min_rating
        AND id(n) IN c1
        AND NOT (m)-->(n) | {m: m, n: n.id}] AS m2

WITH m1,
    size(c1) AS total_intersections,
    [x IN apoc.coll.toSet(m2) | x.m] AS m2

WITH total_intersections,
    apoc.coll.frequencies(m1 + m2) AS movies

UNWIND movies AS movie
WITH movie.item.title AS title, movie.item.imdb_vote_average AS imdb_vote_average,
    movie.item.imdb_vote_count AS imdb_vote_count, movie.count AS intersections,
    toFloat(movie.count) / toFloat(total_intersections) AS score,
    movie.item.id AS imdb_id,
    'https://www.imdb.com/title/' + toString(movie.item.id) AS imdb_url

RETURN title, imdb_id, score, intersections,
    imdb_vote_average, imdb_vote_count, imdb_url
    ORDER BY score DESC, imdb_vote_average DESC, imdb_vote_count DESC
