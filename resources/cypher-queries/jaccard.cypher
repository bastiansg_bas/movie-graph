WITH $imdb_ids AS movies, $similar_ids AS similars 

WITH movies, similars,
    [(m:Movie)-->(n) WHERE m.id IN movies | id(n)] AS n1

WITH similars, n1,
    [(m:Movie)-->()-[:SIMILAR_TO_TAG]->(n)
        WHERE m.id IN movies | id(n)] AS n2

WITH similars,
    apoc.coll.toSet(n1 + n2) AS c1

WITH c1, similars,
    [(m:Movie)-->(n)
        WHERE m.id IN similars
        AND m.imdb_vote_average >= $min_rating | {m: m, n: id(n)}] AS m1

WITH c1, m1,
    [(m:Movie)-->()-[:SIMILAR_TO_TAG]->(n)
        WHERE m.id IN similars
        AND m.imdb_vote_average >= $min_rating
        AND NOT (m)-->(n) | {m: m, n: id(n)}] AS m2

WITH c1, size(c1) AS total_intersections,
    m1 + apoc.coll.toSet(m2) AS movies

UNWIND movies as movie
WITH c1, movie.m AS movie, collect(movie.n) AS c2

WITH movie.title AS title, movie.id AS imdb_id,
    movie.imdb_vote_average AS imdb_vote_average, movie.imdb_vote_count AS imdb_vote_count,
    gds.alpha.similarity.jaccard(c1, c2) AS score

WITH title, imdb_id, imdb_vote_average, imdb_vote_count,
    avg(score) AS score,
    'https://www.imdb.com/title/' + toString(imdb_id) AS imdb_url

RETURN title, imdb_id, score, imdb_vote_average, imdb_vote_count, imdb_url
    ORDER BY score DESC, imdb_vote_average DESC, imdb_vote_count DESC
