import os
import sys
import json


CONFIG_FILE_PATH = '.vscode/settings.json'
BASE_CONFIG = {
    'python.linting.pylintEnabled': True,
    'python.linting.pycodestyleEnabled': True,
    'python.linting.enabled': True,
    'python.linting.flake8Enabled': True,
    'python.linting.pylintArgs': ['--generate-members']
}


def main():
    pipenv_path = f'{sys.argv[1]}/bin/python'
    BASE_CONFIG['python.pythonPath'] = pipenv_path
    os.makedirs(os.path.dirname(CONFIG_FILE_PATH), exist_ok=True)
    with open(CONFIG_FILE_PATH, 'w+') as f:
        f.write(json.dumps(BASE_CONFIG, indent=4))


if __name__ == '__main__':
    main()
