import sys

from subprocess import check_call
from distutils.core import run_setup


def main():
    setup_file_path = sys.argv[1]
    result = run_setup(setup_file_path, stop_after='init')
    requirements = result.install_requires
    if not requirements:
        return

    check_call(['pip', 'install', *requirements])


if __name__ == '__main__':
    main()
