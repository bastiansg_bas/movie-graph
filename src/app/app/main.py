import sys
import time
import logging

from typing import List
from fastapi import FastAPI

from app.models import RequestMovies
from app.recommender.recommender import Recommender


logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)


recommender = Recommender()
app = FastAPI()


@app.post('/movie_recommender/')
async def movie_recommender(movies: RequestMovies) -> List[dict]:
    start = time.time()
    recommended_movies = recommender.get_recommended_movies(
        movies.imdb_ids,
        movies.metric,
        movies.min_rating,
        movies.limit
    )

    process_time = time.time() - start
    logger.info(f'process_time => {process_time}')

    return recommended_movies
