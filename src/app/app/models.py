import os

from typing import List
from pydantic import BaseModel, Field, validator


MIN_LIMIT = os.getenv('MAX_LIMIT', 50)
MAX_LIMIT = os.getenv('MAX_LIMIT', 100)
MAX_MOVIES = os.getenv('MAX_LIMIT', 5)


class RequestMovies(BaseModel):
    imdb_ids: List[str] = Field(
        description='list of movie ids',
        example=[
            'tt0816692',
            'tt0118884',
            'tt0062622'
        ]
    )

    metric: str = Field(
        description='metric of similarity used in graph',
        example='jaccard'
    )

    min_rating: float = Field(
        description='min movie raiting',
        example=7.0
    )

    limit: int = Field(
        description='limit of returned movies',
        example=100
    )

    @validator('imdb_ids')
    def imdb_ids_validator(cls, v):
        if not (len(v) >= 1 and len(v) <= MAX_MOVIES):
            raise ValueError(f'imdb_ids must be an array with \
            min_length 1 and max_length {MAX_MOVIES}')
        return v

    @validator('metric')
    def metric_validator(cls, v):
        if v not in ['intersection', 'jaccard']:
            raise ValueError('metric must be intersection or jaccard')
        return v

    @validator('min_rating')
    def min_rating_validator(cls, v):
        if not (v >= 0 and v <= 10):
            raise ValueError('min_rating must be a float \
            in the range [0, 10]')
        return v

    @validator('limit')
    def limit_validator(cls, v):
        if not (v >= MIN_LIMIT and v <= MAX_LIMIT):
            raise ValueError(f'min_rating must be a int \
            in the range [{MIN_LIMIT}, {MAX_LIMIT}]')
        return v
