import os
from glob import glob


CYPHER_QUERY_PATH = os.getenv('CYPHER_QUERY_PATH', '/resources/cypher-queries')


def load_cypher(cypher_file: str) -> str:
    with open(cypher_file, 'r') as f:
        cypher_query = f.read()
        cypher_query = cypher_query.replace('\n', ' ')
        cypher_query = ' '.join(cypher_query.split())
        return cypher_query


def load_cypher_queries() -> dict:
    cypher_files = glob(f'{CYPHER_QUERY_PATH}/*.cypher')
    cypher_queries = {
        os.path.basename(cypher_file).split('.')[0]: load_cypher(cypher_file)
        for cypher_file in cypher_files
    }

    return cypher_queries
