import logging
import numpy as np

from typing import List
from sklearn.metrics import pairwise_distances

from movie_graph.drivers.neo import NeoDriver
from movie_graph.utils.h5_data import load_h5
from movie_graph.utils.json_data import load_json

from app.recommender.cypher_utils import load_cypher_queries


logger = logging.getLogger(__name__)


CYPHER_QUERIES = '/resources/cypher-queries'
EMBEDDINGS_BASE_PATH = '/resources/movie-embeddings'
MOVIE_VECTORS_PATH = f'{EMBEDDINGS_BASE_PATH}/embeddings.h5'
MOVIE_IDS_PATH = f'{EMBEDDINGS_BASE_PATH}/ids.json'


class Recommender():
    def __init__(self):
        self.neo_driver = NeoDriver()
        self.cypher_queries = load_cypher_queries()
        self.movie_vectors = load_h5(MOVIE_VECTORS_PATH, 'embeddings')
        self.movie_ids = load_json(MOVIE_IDS_PATH)
        logger.info(f'movie_vectors => {self.movie_vectors.shape}')

    def get_most_similars(
            self,
            imdb_ids: List[str],
            limit: int) -> List[str]:

        idx = np.in1d(self.movie_ids, imdb_ids).nonzero()[0]
        movie_vectors = self.movie_vectors[idx]
        average_vector = np.average(movie_vectors, axis=0)
        average_vector = np.expand_dims(average_vector, axis=0)
        similarities = pairwise_distances(
            average_vector,
            self.movie_vectors,
            metric='cosine'
        )

        movies = map(
            lambda x: {
                'movie_id': x[0],
                'similarity': x[1]
            }, zip(self.movie_ids, similarities[0])
        )

        n_movies = len(imdb_ids)
        movies = sorted(movies, key=lambda x: x['similarity'])
        most_simialrs = [x['movie_id'] for x in movies]
        most_simialrs = most_simialrs[n_movies: limit + n_movies]
        return most_simialrs

    def run_query(
            self,
            tx: object,
            base_query: str,
            imdb_ids: list,
            similar_ids: list,
            min_rating: int) -> str:

        records = tx.run(
            base_query,
            imdb_ids=imdb_ids,
            similar_ids=similar_ids,
            min_rating=min_rating
        )

        records = [x.data() for x in records]
        return records

    def get_recommended_movies(
            self,
            imdb_ids: List[str],
            metric: str,
            min_rating: int,
            limit: int) -> List[dict]:

        most_similars = self.get_most_similars(imdb_ids, limit)
        base_query = self.cypher_queries[metric]
        with self.neo_driver.driver.session() as session:
            recommended_movies = session.read_transaction(
                self.run_query,
                base_query,
                imdb_ids,
                most_similars,
                min_rating
            )

        return recommended_movies
