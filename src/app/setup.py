from setuptools import find_packages, setup

setup(
    name='app',
    packages=find_packages(),
    version='1.0.0',
    description='movie_graph app',
    author='Bas',
    author_email='bastiansg.bas@gmail.com',
    url='https://gitlab.com/bastiansg_bas/movie-graph',
    install_requires=[
        'fastapi==0.65.2',
        'uvicorn==0.14.0',
        'pydantic==1.8.2',
    ],
    package_data={'': ['*.yml', '*.yaml']},
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3"
    ]
)
