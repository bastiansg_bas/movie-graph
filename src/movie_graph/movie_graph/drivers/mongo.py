import os
import logging

from time import sleep
from typing import List
from pymongo import MongoClient


logger = logging.getLogger(__name__)


MONGO_URI = os.getenv('MONGO_URI', 'mongodb://localhost:27017')


class MongoDriver():
    def __init__(self):
        self.client = self.get_client(MONGO_URI)
        self.db = self.client.movie_graph
        self.collection = self.db.movies
        self.check_connection()

    def get_client(self, mongo_uri: str) -> MongoClient:
        try:
            client = MongoClient(mongo_uri)
            return client

        except Exception as err:
            logger.error(f'mongo_driver => {err}')
            logger.info('mongodb not ready, waiting...')
            sleep(20)

        return self.get_client(mongo_uri)

    def check_connection(self):
        server_info = self.client.server_info()
        logger.info(f'server_info => {server_info["version"]}')

    def update_movie_data(self, doc: dict):
        filter_ = {'_id': doc.pop('imdb_id')}
        update = {'$set': doc}
        self.collection.update_one(filter_, update, upsert=True)

    def get_docs_by_field(self, field_name: str, id_field: str) -> List[dict]:
        query = {
            field_name: {'$exists': True}
            }

        filter_ = {id_field: 1}
        result = self.collection.find(query, filter_)
        ids = [x[id_field] for x in result]

        return ids

    def get_movie_docs(self) -> List[dict]:
        query = {'title': {'$exists': True}}
        docs = self.collection.find(query)
        docs = list(docs)
        return docs
