import os
import logging

from tqdm.auto import tqdm
from neo4j import GraphDatabase
from typing import List, Iterator
from more_itertools import chunked
from joblib import Parallel, delayed


NEO_URI = os.getenv('NEO_URI', 'bolt://localhost:7687')
NEO_AUTH = os.getenv('NEO_AUTH', None)


logger = logging.getLogger(__name__)


class NeoDriver():
    def __init__(self, n_jobs: int = -1, neo_batch_size: int = 1024):
        self.n_jobs = n_jobs
        self.neo_batch_size = neo_batch_size
        self.driver = GraphDatabase.driver(
            NEO_URI,
            auth=NEO_AUTH,
            max_connection_lifetime=-1
        )

        self.check_connection()

    def check_connection(self):
        with self.driver.session() as session:
            session.run('MATCH () RETURN 1 LIMIT 1')

    def run_transaction(self, queries: List[str]):
        with self.driver.session() as session:
            tx = session.begin_transaction()
            for query in queries:
                try:
                    tx.run(query)

                except Exception as err:
                    logger.error(f'transaction fail with query => {query}')
                    raise err

            tx.commit()

    def run_transactions(self, queries: Iterator[str]):
        logger.info(f'batch_size => {self.neo_batch_size}')
        chunks = list(chunked(queries, self.neo_batch_size))
        for chunk in tqdm(chunks):
            self.run_transaction(chunk)

    def run_parallel_transactions(self, queries: Iterator[str]):
        logger.info(f'batch_size => {self.neo_batch_size}')
        query_chunks = list(chunked(queries, self.neo_batch_size))
        Parallel(n_jobs=self.n_jobs, backend='threading')(
            delayed(self.run_transaction)(x)
            for x in tqdm(query_chunks)
        )

    def get_query_results(self, query: str) -> List[dict]:
        with self.driver.session() as session:
            result = session.run(query)
            result = [x.data() for x in result]
            return result
