from typing import Any


def default_transform(column_value: Any) -> Any:
    return column_value


transforms = {
    'default': default_transform,
}
