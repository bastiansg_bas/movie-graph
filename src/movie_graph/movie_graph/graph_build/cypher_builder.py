from itertools import chain
from typing import Iterator
from more_itertools import flatten, unique_everseen

from .rel_mapper import mapper
from .text import cypher_prop_formatter


def get_node_queries(node: dict) -> dict:
    node_id = cypher_prop_formatter({'id': node['id']})
    node_type = node['type']
    node_props = cypher_prop_formatter(node['props'])

    node_query = f"CREATE (n:{node_type} {node_id}) \
        SET n += {node_props}"

    return node_query


def get_rel_queries(rel: dict) -> dict:
    source_id = cypher_prop_formatter({'id': rel['source_id']})
    source_type = rel['source_type']

    target_id = cypher_prop_formatter({'id': rel['target_id']})
    target_type = rel['target_type']

    rel_type = rel['type']
    rel_props = cypher_prop_formatter(rel['props'])

    rel_query = f"MATCH (n:{source_type} {source_id}), \
       (m: {target_type} {target_id}) \
        CREATE (n)-[:{rel_type} {rel_props}]->(m)"

    return rel_query


def get_constraint_query(node_type: str) -> str:
    query = f"CREATE CONSTRAINT IF NOT EXISTS \
        ON (n:{node_type}) ASSERT n.id IS UNIQUE"
    return query


def get_constraint_queries() -> Iterator[str]:
    mapper_values = list(flatten(mapper.values()))
    source_types = map(lambda x: x['node_type'], mapper_values)

    targets = map(lambda x: x['targets'], mapper_values)
    target_types = map(lambda x: x['node_type'], flatten(targets))

    node_types = unique_everseen(chain(source_types, target_types))
    constraint_queries = map(get_constraint_query, node_types)
    return constraint_queries
