from itertools import repeat
from collections import ChainMap
from typing import List, Iterator
from more_itertools import flatten, unzip

from movie_graph.utils.json_data import group_by_key

from .rel_mapper import mapper
from .column_transforms import transforms


def column_transform(column_item: dict, data_item: dict) -> str:
    t = transforms[column_item['column_transform']]
    transformed = t(data_item.get(column_item['column_name']))
    return transformed


def props_transform(props: List[dict], data_item: dict) -> List[dict]:
    props = map(
        lambda x: {x['column_name']: column_transform(x, data_item)},
        props
    )

    props = dict(ChainMap(*props))
    return props


def transform_node_data(
        rel_map: dict,
        data_item: dict,
        node_type: str) -> dict:

    node_id = column_transform(rel_map['id'], data_item)
    node_props = props_transform(rel_map['node_props'], data_item)

    node_data = {
        f'{node_type}_id': node_id,
        f'{node_type}_type': rel_map['node_type'],
        f'{node_type}_props': node_props,
    }

    if node_type != 'target':
        return node_data

    rel_props = props_transform(rel_map['rel_props'], data_item)
    rel_data = {
        'rel_type': rel_map['rel_type'],
        'rel_props': rel_props
    }

    node_data.update(rel_data)
    return node_data


def get_rels(rel_map: dict, data_item: dict) -> Iterator[dict]:
    source_data = transform_node_data(rel_map, data_item, 'source')
    target_data = map(
        lambda x: transform_node_data(x, data_item, 'target'),
        rel_map['targets']
    )

    rels = map(lambda x: {**source_data, **x}, target_data)
    rels = filter(lambda x: x['source_id'] and x['target_id'], rels)
    return rels


def get_rel_data(data_item: dict) -> Iterator[dict]:
    file_map = mapper[data_item['file_name']]
    column_names = map(lambda x: x['id']['column_name'], file_map)
    valid_keys = set(column_names) & set(data_item.keys())

    rel_maps = filter(lambda x: x['id']['column_name'] in valid_keys, file_map)
    rel_data = map(get_rels, rel_maps, repeat(data_item))
    rel_data = flatten(rel_data)

    return rel_data


# ###################### split for parallelization ###################### #


def split_rel_data(rel_data_item: dict) -> dict:
    src_node = {
        'id': rel_data_item['source_id'],
        'type': rel_data_item['source_type'],
        'props': rel_data_item['source_props']
    }

    trg_node = {
        'id': rel_data_item['target_id'],
        'type': rel_data_item['target_type'],
        'props': rel_data_item['target_props']
    }

    rel = {
        'type': rel_data_item['rel_type'],
        'props': rel_data_item['rel_props'],
        'source_id': rel_data_item['source_id'],
        'target_id': rel_data_item['target_id'],
        'source_type': rel_data_item['source_type'],
        'target_type': rel_data_item['target_type']
    }

    nodes = [src_node, trg_node]
    return nodes, rel


def merge_node_props(nodes: Iterator[dict]):
    def merge_props(nodes: List[dict]) -> dict:
        props = map(lambda x: x.pop('props'), nodes)
        merged_node = nodes[0]
        merged_node['props'] = dict(ChainMap(*props))
        return merged_node

    node_id_groups = group_by_key(nodes, 'id')
    merged_nodes = map(merge_props, node_id_groups)
    return merged_nodes


def merge_nodes(nodes: Iterator[dict]) -> Iterator[dict]:
    nodes_type_groups = group_by_key(nodes, 'type')
    merge_nodes = map(merge_node_props, nodes_type_groups)
    merge_nodes = flatten(merge_nodes)
    return merge_nodes


def get_graph_data(data: Iterator[dict]) -> tuple:
    rel_data = map(get_rel_data, data)
    rel_data = flatten(rel_data)

    splited_rel_data = map(split_rel_data, rel_data)
    nodes, rels = unzip(splited_rel_data)
    nodes = merge_nodes(flatten(nodes))

    return nodes, rels
