import os
import sys
import logging

from tqdm.auto import tqdm
from typing import Iterator, List
from more_itertools import flatten, unique_everseen, random_permutation

from .graph_data_builder import get_graph_data
from .cypher_builder import (
    get_node_queries,
    get_rel_queries,
    get_constraint_queries
)

from movie_graph.drivers.neo import NeoDriver
from movie_graph.utils.json_data import load_json, get_unique


logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)


class NeoBuilder():
    def __init__(self, n_jobs: int = -1, batch_size: int = 1024):
        self.njobs = n_jobs
        self.batch_size = batch_size
        self.neo_driver = NeoDriver()

    def _load_data(self, file_path: str) -> List[dict]:
        data = load_json(file_path)
        file_name = os.path.basename(file_path).split('.')[0]
        for data_item in data:
            data_item['file_name'] = file_name

        data = get_unique(data)
        return data

    def load_data(self, file_paths: List[str]) -> Iterator[str]:
        data = map(self._load_data, file_paths)
        data = flatten(data)
        return data

    def unique_and_shuffle(self, queries: Iterator[str]) -> Iterator[str]:
        queries = unique_everseen(queries)
        queries = random_permutation(queries)
        return queries

    def get_graph_queries(self, file_paths: List[str]) -> tuple:
        logger.info('creating graph data')
        data = self.load_data(tqdm(file_paths))
        nodes, rels = get_graph_data(data)

        node_queries = map(get_node_queries, nodes)
        node_queries = self.unique_and_shuffle(node_queries)

        rel_queries = map(get_rel_queries, rels)
        rel_queries = self.unique_and_shuffle(rel_queries)

        return node_queries, rel_queries

    def build_graph(self, file_paths: List[str]):
        logger.info('creating id constraints')
        constraint_queries = get_constraint_queries()
        self.neo_driver.run_transaction(constraint_queries)

        node_queries, rel_queries = self.get_graph_queries(file_paths)

        logger.info('creating nodes')
        self.neo_driver.run_parallel_transactions(node_queries)

        logger.info('creating relations')
        self.neo_driver.run_parallel_transactions(rel_queries)
