mapper = {
    'movies-merged': [
        {
            'id': {
                'column_name': 'imdb_id',
                'column_transform': 'default'
            },
            'node_type': 'Movie',
            'node_props': [
                {
                    'column_name': 'title',
                    'column_transform': 'default'
                },
                {
                    'column_name': 'original_title',
                    'column_transform': 'default'
                },
                {
                    'column_name': 'tmdb_id',
                    'column_transform': 'default'
                },
                {
                    'column_name': 'tmdb_vote_average',
                    'column_transform': 'default'
                },
                {
                    'column_name': 'tmdb_vote_count',
                    'column_transform': 'default'
                },
                {
                    'column_name': 'imdb_vote_average',
                    'column_transform': 'default'
                },
                {
                    'column_name': 'imdb_vote_count',
                    'column_transform': 'default'
                },
                {
                    'column_name': 'overview',
                    'column_transform': 'default'
                },
                {
                    'column_name': 'posters',
                    'column_transform': 'default'
                },
                {
                    'column_name': 'backdrops',
                    'column_transform': 'default'
                },
                {
                    'column_name': 'popularity',
                    'column_transform': 'default'
                }
            ],
            'targets': [
                {
                    'id': {
                        'column_name': 'year',
                        'column_transform': 'default'
                    },
                    'node_type': 'Year',
                    'node_props': [],
                    'rel_type': 'FROM_YEAR',
                    'rel_props': []
                }
            ]
        }
    ],

    'cast': [
        {
            'id': {
                'column_name': 'id',
                'column_transform': 'default'
            },
            'node_type': 'Person',
            'node_props': [
                {
                    'column_name': 'name',
                    'column_transform': 'default'
                }
            ],
            'targets': [
                {
                    'id': {
                        'column_name': 'imdb_id',
                        'column_transform': 'default'
                    },
                    'node_type': 'Movie',
                    'node_props': [],
                    'rel_type': 'ACTED_IN',
                    'rel_props': []
                }
            ]
        }
    ],

    'crew': [
        {
            'id': {
                'column_name': 'id',
                'column_transform': 'default'
            },
            'node_type': 'Person',
            'node_props': [
                {
                    'column_name': 'name',
                    'column_transform': 'default'
                }
            ],
            'targets': [
                {
                    'id': {
                        'column_name': 'imdb_id',
                        'column_transform': 'default'
                    },
                    'node_type': 'Movie',
                    'node_props': [],
                    'rel_type': 'MADE',
                    'rel_props': []
                }
            ]
        }
    ],

    'likes': [
        {
            'id': {
                'column_name': 'userId',
                'column_transform': 'default'
            },
            'node_type': 'Person',
            'node_props': [],
            'targets': [
                {
                    'id': {
                        'column_name': 'imdb_id',
                        'column_transform': 'default'
                    },
                    'node_type': 'Movie',
                    'node_props': [],
                    'rel_type': 'LIKES',
                    'rel_props': []
                }
            ]
        }
    ],

    'genome-tags': [
        {
            'id': {
                'column_name': 'imdb_id',
                'column_transform': 'default'
            },
            'node_type': 'Movie',
            'node_props': [],
            'targets': [
                {
                    'id': {
                        'column_name': 'id',
                        'column_transform': 'default'
                    },
                    'node_type': 'Tag',
                    'node_props': [
                        {
                            'column_name': 'name',
                            'column_transform': 'default'
                        }
                    ],
                    'rel_type': 'HAS_TAG',
                    'rel_props': []
                }
            ]
        }
    ],

    'keywords': [
        {
            'id': {
                'column_name': 'imdb_id',
                'column_transform': 'default'
            },
            'node_type': 'Movie',
            'node_props': [],
            'targets': [
                {
                    'id': {
                        'column_name': 'id',
                        'column_transform': 'default'
                    },
                    'node_type': 'Tag',
                    'node_props': [
                        {
                            'column_name': 'name',
                            'column_transform': 'default'
                        }
                    ],
                    'rel_type': 'HAS_TAG',
                    'rel_props': []
                }
            ]
        }
    ],

    'genres': [
        {
            'id': {
                'column_name': 'imdb_id',
                'column_transform': 'default'
            },
            'node_type': 'Movie',
            'node_props': [],
            'targets': [
                {
                    'id': {
                        'column_name': 'id',
                        'column_transform': 'default'
                    },
                    'node_type': 'Genre',
                    'node_props': [
                        {
                            'column_name': 'name',
                            'column_transform': 'default'
                        }
                    ],
                    'rel_type': 'HAS_GENRE',
                    'rel_props': []
                }
            ]
        }
    ]
}
