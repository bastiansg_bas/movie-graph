import re
import datetime

from typing import Any


def prop_key_normalize(prop: str) -> str:
    prop = re.sub(r'\(|\)', '', prop)
    prop = prop.replace(' ', '_')
    prop = prop.lower()
    return prop


def prop_formatter(key: str, value: Any) -> str:
    key = prop_key_normalize(key)

    if isinstance(value, str):
        formatted = f'{key}:"{value}"'
        return formatted

    if isinstance(value, list):
        formatted = f"{key}:{value}"
        formatted = formatted.replace("'", '"')
        return formatted

    if isinstance(value, datetime.datetime):
        data_string = value.strftime('%Y-%m-%d')
        formatted = f'{key}:date("{data_string}")'
        return formatted

    formatted = f"{key}:{value}"
    return formatted


def cypher_prop_formatter(props: dict) -> str:
    props = {k: v for k, v in props.items() if v is not None}
    formatted = map(lambda x: prop_formatter(*x), props.items())
    formatted = ', '.join(formatted)
    formatted = f"{{{formatted}}}"
    return formatted
