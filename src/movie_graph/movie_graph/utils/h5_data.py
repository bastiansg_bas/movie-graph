import h5py
import numpy as np


def load_h5(h5_file_path: str, h5_key: str) -> np.ndarray:
    with h5py.File(h5_file_path, 'r') as h5f:
        vectors = np.array(h5f.get(h5_key))
        return vectors


def save_h5(vectors: np.ndarray, h5_file_path: str, h5_key: str):
    with h5py.File(h5_file_path, 'w') as h5f:
        h5f.create_dataset(h5_key, data=vectors)
