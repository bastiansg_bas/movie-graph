import json

from itertools import groupby
from typing import List, Union, Iterator, Iterable


def save_json(json_data: Union[dict, List[dict]], file_path: str):
    with open(file_path, 'w') as f:
        f.write(json.dumps(json_data, indent=4, ensure_ascii=False))


def load_json(json_file_path: str) -> Union[dict, List[dict]]:
    with open(json_file_path, 'r') as f:
        content = json.loads(f.read())
        return content


def get_unique(json_list: List[dict]) -> Iterator[dict]:
    unique = set(map(json.dumps, json_list))
    unique = map(json.loads, unique)
    return unique


def group_by_key(json_iter: Iterable[dict], key: str) -> Iterator[list]:
    def lambda_func(x):
        return str(x[key])

    sorted_ = sorted(json_iter, key=lambda_func)
    groups = map(lambda x: list(x[1]), groupby(sorted_, key=lambda_func))
    return groups
