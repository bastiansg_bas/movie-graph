import logging

import numpy as np
import tensorflow_hub as hub
import tensorflow_text as text  # noqa

from typing import List
from tqdm.auto import tqdm
from more_itertools import chunked, flatten
from tensorflow.python.framework.ops import EagerTensor


logging.disable(logging.WARNING)


class Encoder():
    def __init__(self):
        self.preprocessor = hub.KerasLayer(
            'https://tfhub.dev/google/universal-sentence-encoder-cmlm/multilingual-preprocess/2')  # noqa

        self.encoder = hub.KerasLayer(
            'https://tfhub.dev/google/universal-sentence-encoder-cmlm/multilingual-base-br/1')  # noqa

    def embedding_normalize(self, embeds: EagerTensor) -> np.ndarray:
        norms = np.linalg.norm(embeds, 2, axis=1, keepdims=True)
        normalized = embeds / norms
        normalized = normalized.numpy()
        return normalized

    def encode(self, sentences: List[str]) -> np.ndarray:
        embeds = self.encoder(self.preprocessor(sentences))['default']
        embeds = self.embedding_normalize(embeds)
        return embeds

    def batch_encode(
            self,
            sentences: List[str],
            batch_size: int = 128) -> np.ndarray:

        chunks = list(chunked(sentences, batch_size))
        embeds = map(self.encode, tqdm(chunks))
        embeds = np.array(list(flatten(embeds)))
        return embeds
