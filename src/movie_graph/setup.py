from setuptools import find_packages, setup

setup(
    name='movie_graph',
    packages=find_packages(),
    version='1.0.0',
    description='yet another movie recommender',
    author='Bas',
    author_email='bastiansg.bas@gmail.com',
    url='https://gitlab.com/bastiansg_bas/movie-graph',
    install_requires=[
        'pandas==1.2.4',
        'more-itertools==8.8.0',
        'joblib==1.0.1',
        'neo4j==4.3.1',
        'tqdm==4.61.1',
        'tensorflow==2.4.2',
        'tensorflow_hub==0.12.0',
        'tensorflow_text==2.4.3',
        'aiohttp==3.7.4',
        'beautifulsoup4==4.9.3',
        'pymongo==3.11.4',
        'scikit-learn==0.24.2'
    ],
    package_data={'': ['*.yml', '*.yaml']},
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3"
    ]
)
